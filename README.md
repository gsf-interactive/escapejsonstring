# EscapeJsonString

pipe contents of .json as follows

```
cat ./some_file.json | EscapeJsonString
```

Program will output stringified JSON (white space removed and characters escaped)

Transforms this

```json
{
  "property1": "this is the value of the property",
  "property2": 512,
  "property3": "another property here",
  "property4": 222,
  "property5": "this is \"quoted\""
}
```

into this

```
{\"property1\":\"this is the value of the property\",\"property2\":512,\"property3\":\"another property here\",\"property4\":222,\"property5\":\"this is \\\"quoted\\\"\"}
```
