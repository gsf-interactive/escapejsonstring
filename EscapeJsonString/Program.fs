﻿open Newtonsoft.Json
open Newtonsoft.Json.Linq
open System
open System.Text.RegularExpressions

// escapes any already escaped characters
let rec escapeTheEscapes (startIndex : int) (json : string) =
    match json.IndexOf("\\", startIndex) with
    | -1 -> json
    | indexOfEscape -> 
        json.Insert(indexOfEscape, "\\\\")
        |> escapeTheEscapes (indexOfEscape+4)
        
// Minify Json using Newtonsoft
let minifyJson json =
    JObject.Parse json
    |> fun i -> i.ToString(Formatting.None)

// escape quotes
let escapeJson json =
    escapeTheEscapes 0 json
    |> fun s -> Regex.Replace(s, "(?<!\\\\)\"", "\\\"")

[<EntryPoint>]
let main argv =
    Console.In.ReadToEnd()
    |> minifyJson
    |> escapeJson
    |> printfn "%s"
    0
